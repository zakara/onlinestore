
public class MainClass {
	public static void main(String[] args) {

		EmpDetails details = new EmpDetails("Juan Dela Cruz", "101", "Math", 6000.00);
		details.setDesignation("Teacher");

		details.compute();
		details.showEmpDetails();
		details.showDesignation();
	}
}

/*
 * https://medium.com/@alifabdullah/easiest-explanation-of-abstract-class-and-
 * interface-280741bc2daf
 */