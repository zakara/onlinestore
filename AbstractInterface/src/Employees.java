
public abstract class Employees implements I_Salary {

	private String designation;

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public void showDesignation() {
		System.out.println("Designation is :" + getDesignation());
	}

	public abstract String name(String name);

	public abstract String empId(String empId);

	public abstract double salary(double salary);

	public abstract String subject(String subject);
}
