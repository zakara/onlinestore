
public class EmpDetails extends Employees {

	private String name;
	private String empId;
	private String subject;
	private double salary;
	private double totalSalary;

	public EmpDetails(String string2, String id, String subj, double sal) {
		this.name = string2;
		this.empId = id;
		this.subject = subj;
		this.salary = sal;
	}

	public void showEmpDetails() {
		System.out.println("Name : " + getName() + "\nEmp ID :" + getEmpId() + "\nSubject :" + getSubject()
				+ "\nSalary :" + getTotalSalary());
	}

	@Override
	public String name(String name) {
		return null;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmpId() {
		return empId;
	}

	public void setEmpId(String empId) {
		this.empId = empId;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	@Override
	public String empId(String empId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public double salary(double salary) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String subject(String subject) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void compute() {

		System.out.println(getSalary());
		if (getSalary() < 5000) {
			setTotalSalary(getSalary() * 2);
		} else if (getSalary() < 10000) {
			setTotalSalary(getSalary() * 3);
		} else {
			setTotalSalary(getSalary() * 4);
		}
	}

	public double getTotalSalary() {
		return totalSalary;
	}

	public void setTotalSalary(double totalSalary) {
		this.totalSalary = totalSalary;
	}
}
